/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "bus_link.hpp"

#include <boost/container/small_vector.hpp>
#include <string_view>

#include "libs/cppzmq/zmq_addon.hpp"

#include <pizza-config/service.hpp>

namespace io = boost::asio;
using error_code = boost::system::error_code;
using system_error = boost::system::system_error;

namespace pizza
{

bus_link::bus_link(zmq::context_t& ctx)
: sub_(ctx, zmq::socket_type::sub)
, pub_(ctx, zmq::socket_type::pub)
, publish_inproc_(ctx)
, command_inproc_(ctx)
, on_error_(pizza::default_network_error_callback())
, ctx_(ctx)
{}

bus_link::~bus_link()
{
    stop();
}

bus_link& bus_link::set_error_handler(network_error_callback cb)
{
    on_error_ = std::move(cb);
    return *this;
}

void bus_link::set_security(const service& cfg)
{
    if (cfg.encryption()) {
        set_security(*cfg.bus_pub_public_key(), *cfg.bus_sub_public_key());
    }
}

void bus_link::set_security(std::string pub_key, std::string sub_key)
{
    pub_.set(zmq::sockopt::curve_serverkey, pub_key);
    install_keypair(pub_);
    sub_.set(zmq::sockopt::curve_serverkey, sub_key);
    install_keypair(sub_);
}

constexpr bool bus_link::running() const noexcept
{
    return running_;
}

bus_link& bus_link::unsubscribe(std::string topic)
{
    if (!running_) {
        subscriptions_.erase(topic);
        sub_.set(zmq::sockopt::unsubscribe, topic);
    }
    else {
        std::unique_lock<std::mutex> locker(subscriptions_locker_);
        enqueued_unsubscriptions_.emplace_back(std::move(topic));
        locker.unlock();
        notify_command();
    }
    return *this;
}

bus_link& bus_link::sub_connect(std::string sub_address)
{
    conn_info_.sub_addr = std::move(sub_address);
    conn_info_.sub_connected = true;
    return *this;
}

bus_link& bus_link::pub_connect(std::string pub_address)
{
    conn_info_.pub_addr = std::move(pub_address);
    conn_info_.pub_connected = true;
    return *this;
}

void bus_link::start(boost::asio::thread_pool& thread_pool)
{
    ensure_connected();
    if (running_) return;
    thread_pool_ = &thread_pool;
    running_ = true;
    bus_thread_ = std::thread([this] { mainloop(); });
}

void bus_link::stop()
{
    if (!running_) return;
    running_ = false;
    notify_command();
    bus_thread_.join();
    thread_pool_ = nullptr;
}

void bus_link::mainloop()
{
    try {
        sub_.connect(conn_info_.sub_addr);
        pub_.connect(conn_info_.pub_addr);
    }
    catch (...) {
        on_error_(network_op_type::op_conn, std::current_exception(), "Bus connection failed");
        return;
    }
    auto publish_inproc_listener = decltype(publish_inproc_)::create_listener_socket(ctx_);
    auto command_inproc_listener = decltype(command_inproc_)::create_listener_socket(ctx_);
    zmq::pollitem_t items[] = {
        { sub_, 0, ZMQ_POLLIN, 0},
        { publish_inproc_listener, 0, ZMQ_POLLIN, 0 },
        { command_inproc_listener, 0, ZMQ_POLLIN, 0 }
    };
    while (true) {
        auto n_items = zmq::poll(items, std::size(items));
        if (!running_) return;
        // incoming message
        if (items[0].revents & ZMQ_POLLIN) {
            // topic - message
            recv_type received;
            try {
                if (!zmq::recv_multipart_n(sub_, received.begin(), received.size()))
                    continue;
                dispatch_message(received);
            }
            catch (...) {
                on_error_(network_op_type::op_recv, std::current_exception(), "bus_link");
                continue;
            }
        }
        if (items[1].revents & ZMQ_POLLIN) {
            std::array<zmq::message_t, 3> received;
            try {
                if (!zmq::recv_multipart_n(publish_inproc_listener, received.begin(), received.size()))
                    continue;
                pub_.send(std::move(received[1]), zmq::send_flags::sndmore).value();
                pub_.send(std::move(received[2]), zmq::send_flags::none); 
            }
            catch (...) {
                on_error_(network_op_type::op_send, std::current_exception(), "bus_link");
                continue;
            }
        }
        if (items[2].revents & ZMQ_POLLIN) {
            std::array<zmq::message_t, 2> received;
            try {
                if (!zmq::recv_multipart_n(command_inproc_listener, received.begin(), received.size()))
                    continue;
                dispatch_command();
            }
            catch (...) { continue; }
        }
    }
}

void bus_link::dispatch_message(recv_type& received)
{
    auto shared_topic = std::make_shared<zmq::message_t>(std::move(received[0]));
    auto shared_message = std::make_shared<zmq::message_t>(std::move(received[1]));
    auto topic = shared_topic->to_string_view();
    bool found = false;
    std::lock_guard<std::mutex> _(subscriptions_locker_);
    for (auto it = subscriptions_.begin(); it != subscriptions_.end(); ++it) {
        if (topic.find(it->first) == 0) {
            found = true;
            auto callback = it->second;
            boost::asio::post(*thread_pool_, [shared_topic, shared_message, callback = std::move(callback)] {
                (*callback)(*shared_topic, *shared_message);
            });
        }
    }
    if (!found)
        throw std::runtime_error("got message with undefined topic '" + std::string(topic) + "'");
}

void bus_link::dispatch_command()
{
    std::lock_guard<std::mutex> _(subscriptions_locker_);
    while (!enqueued_subscriptions_.empty()) {
        auto& p = enqueued_subscriptions_.front();
        enqueued_subscriptions_.pop_front();
        sub_.set(zmq::sockopt::subscribe, p.first);
        subscriptions_.emplace(std::move(p.first), std::move(p.second));
    }
    while (!enqueued_unsubscriptions_.empty()) {
        const auto& s = enqueued_unsubscriptions_.front();
        sub_.set(zmq::sockopt::unsubscribe, s);
        subscriptions_.erase(s);
        enqueued_unsubscriptions_.pop_front();
    }
}

void bus_link::ensure_connected()
{
    if (!conn_info_.sub_connected)
        throw std::runtime_error("bus_link not connected to subscriber");
    if (!conn_info_.pub_connected)
        throw std::runtime_error("bus_link not connected to publisher");
}

void bus_link::notify_command()
{
    command_inproc_.notify();
}

}
