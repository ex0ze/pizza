/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "libs/cppzmq/zmq.hpp"

#include <boost/asio.hpp>
#include <boost/container/flat_map.hpp>

#include <google/protobuf/message.h>

#include <atomic>
#include <functional>
#include <shared_mutex>
#include <string>
#include <thread>
#include <vector>

#include "error_handling.hpp"
#include "function_traits.hpp"
#include "inproc_communicator.hpp"
#include "security_layer.hpp"

namespace pizza
{

class bus_link : public detail::security_layer {
public:
    using recv_type = std::array<zmq::message_t, 2>;
    using message_callback_type = std::function<void(const zmq::message_t&, const zmq::message_t&)>;

    bus_link(zmq::context_t& ctx);
    ~bus_link();

    inline bus_link& set_error_handler(network_error_callback cb);

    virtual void set_security(const service& cfg) override;
    void set_security(std::string pub_key, std::string sub_key);

    constexpr bool running() const noexcept;

    //schedule subscription event
    template <typename Callback>
    bus_link& subscribe(std::string topic, Callback callback);

    //schedule unsubscription event
    bus_link& unsubscribe(std::string topic);

    bus_link& sub_connect(std::string sub_address);

    bus_link& pub_connect(std::string pub_address);

    //schedule publish event
    template <typename Message>
    bus_link& async_publish(std::string_view topic, Message && message);

    void start(boost::asio::thread_pool& thread_pool);

    void stop();

private:
    void mainloop();

    void dispatch_message(recv_type& received);

    void dispatch_command();

    void ensure_connected();

    void notify_command();

    zmq::socket_t sub_;
    zmq::socket_t pub_;
    static inline constexpr char publish_inproc_addr[] = "inproc://bus_publish";
    inproc_communicator<publish_inproc_addr> publish_inproc_;
    static inline constexpr char command_inproc_addr[] = "inproc://bus_command";
    inproc_communicator<command_inproc_addr> command_inproc_;
    std::thread bus_thread_;

    boost::container::flat_map<std::string, std::shared_ptr<message_callback_type>> subscriptions_;

    std::deque<std::pair<std::string, std::shared_ptr<message_callback_type>>> enqueued_subscriptions_;
    std::deque<std::string> enqueued_unsubscriptions_;
    std::mutex subscriptions_locker_;

    network_error_callback on_error_;
    zmq::context_t& ctx_;
    // for deferred initialization
    boost::asio::thread_pool* thread_pool_ = nullptr;
    bool running_ = false;

    struct {
        std::string pub_addr, sub_addr;
        bool pub_connected = false,
             sub_connected = false;
    } conn_info_;
};

template <typename Callback>
bus_link& bus_link::subscribe(std::string topic, Callback callback) {
    using Message = std::decay_t<typename meta::function_traits<Callback>::template argument<0>::type>;

    message_callback_type cb = [callback = std::move(callback)]
    (const zmq::message_t& topic, const zmq::message_t& message) {
        // string or string_view
        if constexpr (std::is_same_v<std::string, Message> || std::is_same_v<std::string_view, Message>) {
            callback(topic.to_string_view(),
                Message(static_cast<const char*>(message.data()), message.size()));
        }
        // protobuf case
        else if constexpr (std::is_base_of_v<google::protobuf::Message, Message>) { 
            Message msg;
            if (!msg.ParseFromArray(message.data(), message.size())) return;
            callback(topic.to_string_view(), std::move(msg));
        }
        else {
            static_assert(!sizeof(Message*), "Unsupported subscription type");
        }
    };
    if (!running_) {
        sub_.set(zmq::sockopt::subscribe, topic);
        subscriptions_.emplace(std::move(topic), std::make_shared<message_callback_type>(std::move(cb)));
    }
    else {
        std::unique_lock<std::mutex> locker(subscriptions_locker_);
        enqueued_subscriptions_.emplace_back(std::move(topic), std::make_shared<message_callback_type>(std::move(cb)));
        locker.unlock();
        notify_command();
    }
    return *this;
}

template <typename Message>
bus_link& bus_link::async_publish(std::string_view topic_, Message && message) {
    ensure_connected();
    using Decayed = std::decay_t<Message>;
    zmq::message_t topic, to_send;
    topic.rebuild(topic_.data(), topic_.size());
    if constexpr (std::is_base_of_v<google::protobuf::Message, Decayed>) {
        to_send.rebuild(message.ByteSizeLong());
        message.SerializeToArray(to_send.data(), to_send.size());
    }
    else if constexpr (std::is_same_v<std::string, Decayed>) {
        to_send.rebuild(message.data(), message.size());
    }
    else if constexpr (std::is_convertible_v<Decayed, std::string_view>) {
        auto sv = std::string_view(message);
        to_send.rebuild(sv.data(), sv.size());
    }
    else {
        static_assert(!sizeof(Decayed*), "Unsupported publish type");
    }
    std::vector<zmq::message_t> vec;
    vec.push_back(std::move(topic));
    vec.push_back(std::move(to_send));
    publish_inproc_.send(std::move(vec));
    return *this;
}

}
