/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <future>
#include <azmq/message.hpp>

namespace pizza
{

template <typename Contract>
class contract_future : public std::future<azmq::message_vector>
{
    using parent_t = std::future<azmq::message_vector>;
    using response_t = typename Contract::response_type;
public:
    contract_future(std::future<azmq::message_vector> && v)
        : std::future<azmq::message_vector>(std::move(v)) {}

    response_t get() {
        auto data = parent_t::get();
        if (data.empty())
            throw std::runtime_error("Got empty response");
        response_t response;
        if (!response.ParseFromArray(data[0].data(), data[0].size()))
            throw std::runtime_error("Error deserializing response");
        return response;
    }
};

} // namespace pizza
