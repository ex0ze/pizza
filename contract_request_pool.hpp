/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "contract_future.hpp"
#include "function_traits.hpp"
#include "request_pool.hpp"

namespace pizza
{

class contract_request_pool : public request_pool {
public:
    contract_request_pool(std::size_t pool_size = 50)
    : request_pool(pool_size) {}

    template <typename Message, typename Contract> 
    inline contract_future<Contract> async_request(std::string service_name,
                                                   typename Contract::request_type const & request,
                                                   std::chrono::milliseconds timeout = std::chrono::milliseconds::max())
    {
        Message msg;
        msg.mutable_message()->PackFrom(request);
        return contract_future<Contract>(request_pool::async_request(std::move(service_name), msg.SerializeAsString(), timeout));
    }

    template <typename Message, typename Contract, typename Callback,
    typename std::enable_if_t<
        std::is_same_v<typename Contract::response_type,
            std::decay_t<typename meta::function_traits<Callback>::template argument<0>::type>>>* = nullptr>
    inline void async_request(std::string service_name,
                              typename Contract::request_type const & request,
                              Callback && cb,
                              std::chrono::milliseconds timeout = std::chrono::milliseconds::max())
    {
        using response_type = typename Contract::response_type;
        Message msg;
        msg.mutable_message()->PackFrom(request);
        request_pool::callback_type callback = 
        [cb = std::forward<Callback>(cb)](request_pool::recv_type received, std::exception_ptr ex_ptr) mutable {
            if (ex_ptr) {
                cb(response_type(), ex_ptr);
            }
            else {
                try {
                    if (received.empty())
                        throw std::runtime_error("Got empty response");
                    response_type response;
                    if (!response.ParseFromArray(received[0].data(), received[0].size()))
                        throw std::runtime_error("Error while parsing protobuf");
                    cb(std::move(response), nullptr);
                }
                catch (...) {
                    cb(response_type(), std::current_exception());
                }
            }
        };
        request_pool::async_request(std::move(service_name), msg.SerializeAsString(), std::move(callback), timeout);
    }
};

} // namespace pizza
