/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "libs/cppzmq/zmq.hpp"
#include "libs/cppzmq/zmq_addon.hpp"
#include <pizza-config/service.hpp>

#include <string>
#include <thread>
#include <vector>

#include "error_handling.hpp"
#include "inproc_communicator.hpp"
#include "security_layer.hpp"

namespace pizza
{

template <typename Delegate>
class broker_link : public detail::security_layer {
    using error_code = boost::system::error_code;
    using system_error = boost::system::system_error;
    
public:
    using recv_type = std::vector<zmq::message_t>;
    using send_type = std::vector<zmq::message_t>;
    using shared_send_type = std::shared_ptr<send_type>;

    broker_link(zmq::context_t& ctx, Delegate delegate)
    : ctx_(ctx)
    , socket_(ctx, zmq::socket_type::dealer)
    , inproc_(ctx)
    , on_error_(default_network_error_callback())
    , delegate_(std::move(delegate)) {}

    ~broker_link() { stop(); }

    inline broker_link& connect(const std::string& addr) {
        socket_.connect(addr);
        return *this;
    }

    inline broker_link& set_error_handler(network_error_callback cb) {
        on_error_ = std::move(cb);
        return *this;
    }

    virtual void set_security(const service& cfg) override {
        if (cfg.encryption()) {
            install_keypair(socket_);
            std::string key;
            if (cfg.has_balancer())
                key = *cfg.balancer_public_key();
            else
                key = *cfg.broker_public_key();
            socket_.set(zmq::sockopt::curve_serverkey, key);
        }
    }

    inline broker_link& set_service_name(const std::string& addr) {
        socket_.set(zmq::sockopt::routing_id, addr);
        return *this;
    }
    
    constexpr zmq::socket_t& socket() noexcept { return socket_; }
    constexpr Delegate& delegate() noexcept { return delegate_; }

    void start() {
        using namespace std::chrono_literals;
        if (started_) return;
        started_ = true;
        auto inproc_listener = decltype(inproc_)::create_listener_socket(ctx_);
        zmq::pollitem_t items[] = {
            { socket_, 0, ZMQ_POLLIN, 0 },
            { inproc_listener, 0, ZMQ_POLLIN, 0 }
        };
        while (true) {
            auto n_items = zmq::poll(items, std::size(items));
            if (!started_) return;
            // incoming message
            recv_type received;
            received.reserve(3);
            if (items[0].revents & ZMQ_POLLIN) {
                try {
                    if (!zmq::recv_multipart(socket_, std::back_inserter(received)))
                        continue;
                }
                catch (...) {
                    on_error_(network_op_type::op_recv, std::current_exception(), "broker_link");
                    continue;
                }
                delegate_.on_message(std::move(received));
            }
            if (items[1].revents & ZMQ_POLLIN) {
                // first frame is empty (topic)
                // second frame is identity
                // third frame is message
                zmq::message_t received[3];
                try {
                    if (!zmq::recv_multipart_n(inproc_listener, received, std::size(received)))
                        continue;
                    socket_.send(std::move(received[1]), zmq::send_flags::sndmore);
                    socket_.send(std::move(received[2]), zmq::send_flags::none);
                }
                catch (...) {
                    on_error_(network_op_type::op_send, std::current_exception(), "broker_link");
                    continue;
                }
            }
        }
    }

    void async_send(send_type data) {
        inproc_.send(std::move(data));
    }

    void stop() {
        started_ = false;
        inproc_.notify();
    }

private:
    zmq::context_t& ctx_;
    zmq::socket_t socket_;
    static inline constexpr char inproc_addr[] = "inproc://broker";
    inproc_communicator<inproc_addr> inproc_;
    network_error_callback on_error_;
    Delegate delegate_;
    bool started_ = false;
};

} // namespace pizza
