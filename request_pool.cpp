/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "request_pool.hpp"

#include <pizza-config/service.hpp>

#include "azmq_helpers.hpp"

namespace io = boost::asio;
namespace sys = boost::system;
using error_code = boost::system::error_code;
using system_error = boost::system::system_error;
using system_clock = std::chrono::system_clock;

namespace pizza
{

request_pool::request_pool(std::size_t pool_size)
    : work_guard_(io_.get_executor())
{
    init_sockets(pool_size);
}

request_pool::~request_pool()
{
    stop();
}

request_pool &request_pool::connect(std::string addr)
{
    if (connected_)
        return *this;
    connected_ = true;
    io::dispatch(io_, [this, addr = std::move(addr)] {
        free_sockets_.clear();
        busy_sockets_.resize(socket_pool_.size(), false);
        for (std::size_t socket_idx = 0; socket_idx < socket_pool_.size(); ++socket_idx) {
            do_connect(socket_pool_[socket_idx], addr, false);
            free_sockets_.push_back(socket_idx);
        }
    });
    return *this;
}

void pizza::request_pool::set_recv_type(send_message_context_type& ctx, recv_type & value)
{
    if (is_promise(ctx))
        get_promise(ctx).set_value(std::move(value));
    else
        get_callback(ctx)(std::move(value), nullptr);
}

void pizza::request_pool::set_exception(send_message_context_type& ctx, std::exception_ptr ex_ptr)
{
    if (is_promise(ctx))
        get_promise(ctx).set_exception(ex_ptr);
    else 
        get_callback(ctx)(recv_type(), ex_ptr);
}

void request_pool::cancel_queue()
{
    for (auto& ctx : context_queue_) {
        try {
            throw sys::system_error(error_code(io::error::operation_aborted));
        }
        catch (...) {
            set_exception(ctx, std::current_exception());
        }
    }
}

void request_pool::set_security(const service& cfg)
{
    if (started_)
        throw std::runtime_error("Can't set security options after request_pool start");
    if (cfg.encryption()) {
        set_security(*cfg.broker_public_key());
    }
}

void request_pool::set_security(std::string broker_key)
{
    if (started_)
        throw std::runtime_error("Can't set security options after request_pool start");
    security.encryption = true;
    security.broker_key = std::move(broker_key);
}

void request_pool::start()
{
    if (started_)
        return;
    started_ = true;
    io_thread_ = std::thread([this] { io_.run(); });
}

void request_pool::stop()
{
    if (!started_)
        return;
    started_ = false;
    io::dispatch(io_, [this] {
        context_busy_.notify_one();
        cancel_ops();
        cancel_queue();
        work_guard_.reset();
        io_.stop();
    });
    io_thread_.join();
}

request_pool::send_message_future request_pool::async_request(
    std::string service_name,
    std::string message,
    std::chrono::milliseconds timeout_)
{
    if (!connected_)
        throw std::runtime_error("Attempt to request while not connected");
    send_message_context_type ctx;
    std::get<send_message_ret>(ctx) = send_message_promise();
    auto& [promise, data, timeout] = ctx;

    data = std::make_shared<send_type>();
    data->push_back(std::move(service_name));
    data->push_back(std::move(message));
    timeout = timeout_;
    auto future = get_promise(ctx).get_future();

    if (auto maybe_idx = get_free_socket(); maybe_idx)
        io::post(io_, [this, ctx = std::move(ctx), idx = *maybe_idx]() mutable {
            do_request(std::move(ctx), idx);
        });
    else
        enqueue_message(std::move(ctx));

    return future;
}

void request_pool::async_request(
    std::string service_name,
    std::string message,
    callback_type cb,
    std::chrono::milliseconds timeout)
{
    if (!connected_)
        throw std::runtime_error("Attempt to request while not connected");
    send_message_context_type ctx;
    auto& [callback, data, timeo] = ctx;
    callback = std::move(cb);
    data = std::make_shared<send_type>();
    data->push_back(std::move(service_name));
    data->push_back(std::move(message));
    timeo = timeout;

    if (auto maybe_idx = get_free_socket(); maybe_idx)
        io::post(io_, [this, ctx = std::move(ctx), idx = *maybe_idx]() mutable {
            do_request(std::move(ctx), idx);
        });
    else
        enqueue_message(std::move(ctx));
}

void request_pool::init_sockets(std::size_t pool_size)
{
    socket_pool_.reserve(pool_size);
    timers_.reserve(pool_size);
    for (std::size_t i = 0; i < pool_size; ++i) {
        socket_pool_.emplace_back(io_, true);
        timers_.emplace_back(io_);
    }
}

void request_pool::enqueue_message(send_message_context_type && context)
{
    std::unique_lock<std::mutex> lock(lock_);
    context_busy_.wait(lock, [this] { return context_queue_.size() < queue_capacity || !started_; });
    if (!started_) return;
    context_queue_.push_back(std::move(context));
}

std::optional<request_pool::send_message_context_type>
request_pool::dequeue_message()
{
    std::unique_lock<std::mutex> lock(lock_);
    if (context_queue_.empty())
        return std::nullopt;
    auto ctx = std::move(context_queue_.front());
    context_queue_.pop_front();
    lock.unlock();
    context_busy_.notify_one();
    return ctx;
}

void request_pool::cancel_ops()
{
    std::lock_guard<std::mutex> _(lock_);
    for (std::size_t idx = 0; idx < busy_sockets_.size(); ++idx) {
        if (busy_sockets_[idx])
            socket_pool_[idx].cancel();
        if (timers_[idx].st == timer_type::waiting)
            timers_[idx].timer.cancel();
    }
}

void request_pool::check_queue(std::size_t socket_idx)
{
    if (auto maybe_message = dequeue_message(); maybe_message)
        do_request(std::move(*maybe_message), socket_idx);
    else
        add_free_socket(socket_idx);
}

void request_pool::do_request(send_message_context_type && context, std::size_t socket_idx)
{
    auto& socket = socket_pool_[socket_idx];
    auto& timeout = get_timeout(context);
    bool has_timeout = (timeout != std::chrono::milliseconds::max());
    if (has_timeout)
        set_op_timeout(socket_idx, timeout);
    auto to_send = get_data(context);
    socket.async_send(*to_send,
                     [this, &socket, socket_idx,
                     ctx = std::move(context), has_timeout]
                     (const error_code &ec, std::size_t) mutable {
        try {
            if (ec) {
                if (has_timeout) cancel_op_timeout(socket_idx);
                if (ec == io::error::operation_aborted && has_timeout && op_timed_out(socket_idx)) {
                    throw system_error(io::error::timed_out);
                }
                throw system_error(ec);
            }
            else {
                socket.async_receive(
                    [this, &socket, socket_idx, ctx = std::move(ctx), has_timeout]
                    (const error_code &ec, azmq::message &msg, std::size_t) mutable {
                    try {
                        if (ec) {
                            if (has_timeout) cancel_op_timeout(socket_idx);
                            if (ec == io::error::operation_aborted && has_timeout && op_timed_out(socket_idx)) {
                                throw system_error(io::error::timed_out);
                            }
                            throw system_error(ec);
                        }
                        if (has_timeout) cancel_op_timeout(socket_idx);
                        recv_type received;
                        azmq_helpers::recv_all(socket, std::move(msg), received);
                        set_recv_type(ctx, received);
                        if (!started_) return;
                        check_queue(socket_idx);
                    }
                    catch (...) {
                        if (!started_) return;
                        set_exception(ctx, std::current_exception());
                        do_connect(socket, socket.endpoint(), true);
                        check_queue(socket_idx);
                    }
                });
            }
        }
        catch (...) {
            set_exception(ctx, std::current_exception());
            if (!started_) return;
            do_connect(socket, socket.endpoint(), true);
            check_queue(socket_idx);
        }
    });
}

void request_pool::do_connect(azmq::socket& sock, std::string endpoint, bool reconnect)
{
    if (reconnect)
        sock = socket_type(io_, true);
    if (security.encryption) {
        install_keypair(sock);
        sock.set_option(azmq::socket::curve_serverkey(security.broker_key));
    }
    sock.connect(std::move(endpoint));
}

void request_pool::set_op_timeout(std::size_t idx, std::chrono::milliseconds timeout) {
    auto& t = timers_[idx];
    t.st = timer_type::waiting;
    t.timer.expires_from_now(timeout);
    t.timer.async_wait([this, &t, idx](const error_code& ec) {
        if (ec) {
            t.st = timer_type::free;
            return;
        }
        t.st = timer_type::timed_out;
        socket_pool_[idx].cancel();
    });
}

void request_pool::cancel_op_timeout(std::size_t idx) {
    auto& t = timers_[idx];
    if (t.st == timer_type::waiting)
        t.timer.cancel();
    t.st = timer_type::free;
}

bool request_pool::op_timed_out(std::size_t idx) const noexcept {
    return timers_[idx].st == timer_type::timed_out;
}

std::optional<std::size_t> request_pool::get_free_socket()
{
    std::lock_guard<std::mutex> _(lock_);
    if (free_sockets_.empty())
        return std::nullopt;
    std::size_t idx = free_sockets_.front();
    free_sockets_.pop_front();
    busy_sockets_[idx] = true;
    return idx;
}

void request_pool::add_free_socket(std::size_t idx)
{
    std::lock_guard<std::mutex> _(lock_);
    free_sockets_.push_back(idx);
    busy_sockets_[idx] = false;
}

} // namespace pizza
