/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <boost/asio/thread_pool.hpp>

#include "libs/cppzmq/zmq.hpp"

#include "broker_link.hpp"
#include "bus_link.hpp"
#include "contract_request_pool.hpp"
#include "message_dispatcher.hpp"
#include "service_initialization.hpp"

namespace pizza
{

template <typename Derived, typename Message, typename ... Contracts>
class basic_service {

    using request_pool_type = contract_request_pool;
    using thread_pool_type = boost::asio::thread_pool;

    struct broker_delegate;
    using broker_type = broker_link<broker_delegate>;

    struct dispatcher_delegate;
    using dispatcher_type = message_dispatcher<dispatcher_delegate, Message, Contracts...>;

    struct dispatcher_delegate {
        dispatcher_delegate(basic_service& service) : service_(service) {}
        template <class Request, class Response>
        void process_message(Request && request, Response& response) {
            static_cast<Derived&>(service_).process_message(std::forward<Request>(request), response);
        }
        void message_processed(zmq::message_t&& identity, zmq::message_t&& response) {
            typename broker_type::send_type to_send;
            to_send.push_back(std::move(identity));
            to_send.push_back(std::move(response));
            service_.broker_.async_send(std::move(to_send));
            if (service_.stop_scheduled_) {
                service_.stop_scheduled_ = false;
                boost::asio::post(*service_.thread_pool_, [this] {
                    service_.stop();
                });
            }
        }
    private:
        basic_service& service_;
    };

    struct broker_delegate {
        broker_delegate(basic_service& service) : service_(service) {}
        void on_message(typename broker_type::recv_type && received) {
            if (received.size() != 2)
                return;
            boost::asio::post(*service_.thread_pool_,
                [*this, received = std::move(received)]() mutable {
                    dispatcher_delegate delegate(service_);
                    dispatcher_type dispatcher(delegate);
                    dispatcher.deserialize(std::move(received[0]), std::move(received[1]));
            });
        }
    private:
        basic_service& service_;
    };
    friend struct broker_delegate;

public:
    basic_service()
    : ctx_(1)
    , broker_(ctx_, broker_delegate(*this))
    , bus_(ctx_)
    {}

    virtual void on_start() {}
    virtual void on_stop() {}

    template <typename Contract>
    inline auto async_request(std::string service_name,
                             typename Contract::request_type const & request,
                             std::chrono::milliseconds timeout = std::chrono::milliseconds::max())
    {
        return request_pool_.async_request<Message, Contract>(std::move(service_name), request, timeout);
    }

    template<typename Callback>
    inline Derived& subscribe(std::string topic, Callback cb) {
        bus_.subscribe(std::move(topic), std::move(cb));
        return static_cast<Derived&>(*this);
    }

    template <typename Publishment>
    inline void async_publish(std::string topic, const Publishment & message) {
        bus_.async_publish(std::move(topic), message);
        return static_cast<Derived&>(*this);
    }

    void start() {
        if (running_) return;
        if (!thread_pool_.has_value())
            throw std::runtime_error("Attempt to run with uninitialized thread pool");
        running_ = true;
        on_start();
        // will run in other threads
        // task_queue_.start();
        request_pool_.start();
        if (use_bus_) bus_.start(*thread_pool_);
        // will run in this thread
        broker_.start();
    }

    void stop() {
        if (!running_) return;
        running_ = false;
        on_stop();
        thread_pool_->stop();
        // task_queue_.stop();
        request_pool_.stop();
        if (use_bus_) bus_.stop();
        broker_.stop();
    }

    void schedule_stop() {
        stop_scheduled_ = true;
    }

    Derived& initialize_from_command_line(command_line_args && args) {
        if (running_)
            throw std::runtime_error("Cannot initialize from command line if already running");
        auto print_info = [](
            std::string service_name,
            std::string_view broker_addr,
            std::optional<std::string_view> balancer_addr,
            std::optional<std::pair<std::string_view, std::string_view>> bus_addr,
            bool encryption,
            std::size_t threads
            )
        {
            std::cout << "Service name: " << service_name << "\n"
            << "Broker addr: " << broker_addr << "\n";
            if (balancer_addr)
                std::cout << "Balancer addr: " << *balancer_addr << "\n";
            if (bus_addr) {
                std::cout << "Bus publishers addr: " << bus_addr->first << "\n"
                          << "Bus subscribers addr: " << bus_addr->second << "\n";
            }
            std::cout << "Encryption: " << (encryption ? "enabled" : "disabled") << "\n"
                      << "Threads: " << threads << std::endl;
        };

        if (args.init_from_json()) {
            const auto& cfg = args.config();
            if (cfg.encryption()) {
                request_pool_.set_security(cfg);
                broker_.set_security(cfg);
                bus_.set_security(cfg);
            }
            thread_pool_.emplace(cfg.threads());
            broker_.set_service_name(cfg.name());
            broker_.connect(cfg.has_balancer() ? cfg.balancer_addr() : cfg.broker_addr());
            request_pool_.connect(cfg.broker_addr());
            if (cfg.has_bus()) {
                use_bus_ = true;
                bus_.pub_connect(cfg.bus_publishers_addr())
                    .sub_connect(cfg.bus_subscribers_addr());
            }

            std::optional<std::string_view> balancer_opt;
            if (cfg.has_balancer()) balancer_opt = cfg.balancer_addr();
            std::optional<std::pair<std::string_view, std::string_view>> bus_opt;
            if (cfg.has_bus())
                bus_opt.emplace(cfg.bus_publishers_addr(), cfg.bus_subscribers_addr());

            print_info(cfg.name(),
                cfg.broker_addr(),
                balancer_opt,
                bus_opt,
                cfg.encryption(),
                cfg.threads()
            );
            return static_cast<Derived&>(*this);
        }
        {
            std::optional<std::pair<std::string_view, std::string_view>> bus_opt;
            if (use_bus_)
                bus_opt.emplace(args.bus_publisher_addr(), args.bus_subscriber_addr());
            print_info(args.service_name(),
                args.broker_addr(),
                args.balancer_addr(),
                bus_opt,
                false,
                args.threads());
        }
        thread_pool_.emplace(args.threads());
        broker_.set_service_name(args.service_name());
        broker_.connect(args.consume_balancer_addr());
        request_pool_.connect(args.consume_broker_addr());
        use_bus_ = args.use_bus();
        if (use_bus_)
            bus_.pub_connect(args.consume_bus_publisher_addr())
                .sub_connect(args.consume_bus_subscriber_addr());
        return static_cast<Derived&>(*this);
    }

protected:
    constexpr broker_type& broker() noexcept { return broker_; }
    constexpr request_pool_type& get_request_pool() noexcept { return request_pool_; }
    constexpr bus_link& bus() noexcept { return bus_; }

private:
    zmq::context_t ctx_;
    std::optional<thread_pool_type> thread_pool_;
    request_pool_type request_pool_;
    broker_type broker_;
    bus_link bus_;
    bool running_ = false;
    bool use_bus_ = false;
    bool stop_scheduled_ = false;
};

} // namespace pizza
