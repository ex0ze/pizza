/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "error_handling.hpp"

#include <iostream>
#include <sstream>

namespace
{

void print_op_type(pizza::network_op_type op, std::ostream& os)
{
    switch (op) {
    case pizza::network_op_type::op_recv:
        os << "(receive) ";
        return;
    case pizza::network_op_type::op_send:
        os << "(send) ";
        return;
    case pizza::network_op_type::op_conn:
        os << "(connect) ";
        return;
    default:
        os << "(unknown op) ";
        return;
    }
}

void print_module(std::string_view module_, std::ostream& os)
{
    if (!module_.empty()) {
        os << "[" << module_ << "] ";
    }
}

}

namespace pizza
{

void print_exception_ptr(std::exception_ptr ex_ptr, std::ostream& os)
{
    if (ex_ptr) {
        try {
            std::rethrow_exception(ex_ptr);
        }
        catch (const std::exception& ex) {
            os << "error: " << ex.what();
        }
        catch (...) {
            os << "error: unknown exception";
        }
    }
}

generic_error_callback default_generic_error_callback()
{
    return [](std::exception_ptr ex_ptr, std::string_view module_) {
        std::ostringstream out;
        print_module(module_, out);
        print_exception_ptr(ex_ptr, out);
        std::cerr << out.str() << std::endl;
    };
}

network_error_callback default_network_error_callback()
{
    return [](network_op_type op, std::exception_ptr ex_ptr, std::string_view module_) {
        std::ostringstream out;
        print_module(module_, out);
        print_op_type(op, out);
        print_exception_ptr(ex_ptr, out);
        std::cerr << out.str() << std::endl;
    };
}

}
