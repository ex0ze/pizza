/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "make_from_requester.hpp"

namespace pizza
{

request_pool_map make_request_pool_map(const requester& r, std::size_t request_pool_sockets)
{
    request_pool_map result;
    for (const auto& [name, obj] : r.nested_brokers()) {
        auto pool = std::make_shared<contract_request_pool>(request_pool_sockets);
        if (obj.encryption())
            pool->set_security(obj.public_key());
        pool->connect(obj.addr())
             .start();
        result.emplace(name, std::move(pool));
    }
    return result;
}

bus_map make_bus_map(const requester& r, zmq::context_t& ctx, boost::asio::thread_pool& thread_pool)
{
    bus_map result;
    for (const auto& [name, obj] : r.nested_buses()) {
        auto bus = std::make_shared<bus_link>(ctx);
        if (obj.encryption())
            bus->set_security(obj.pub_public_key(), obj.sub_public_key());
        bus->pub_connect(obj.pub_addr())
            .sub_connect(obj.sub_addr())
            .start(thread_pool);
        result.emplace(name, std::move(bus));
    }
    return result;
}

}
