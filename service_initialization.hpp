/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <string>

#include <pizza-config/service.hpp>

namespace pizza
{

class missing_argument_exception : public std::exception {
public:
    missing_argument_exception(std::string what, std::string help) noexcept : 
        what_(std::move(what)), help_(std::move(help)) {}
    virtual const char * what() const noexcept override {
        return what_.c_str();
    }
    const char * help() const noexcept {
        return help_.c_str();
    }

private:
    std::string what_;
    std::string help_;
};


class command_line_args {
public:
    command_line_args(int argc, const char * const argv[]);

    constexpr const std::string& broker_addr() const noexcept { return broker_addr_; }
    std::string consume_broker_addr() noexcept { return std::move(broker_addr_); }

    constexpr const std::string& balancer_addr() const noexcept { return balancer_addr_; }
    std::string consume_balancer_addr() noexcept { return std::move(balancer_addr_); }

    constexpr const std::string& service_name() const noexcept { return service_name_; }
    std::string consume_service_name() noexcept { return std::move(service_name_); }

    constexpr const std::string& bus_publisher_addr() const noexcept { return bus_pub_addr_; }
    std::string consume_bus_publisher_addr() noexcept { return std::move(bus_pub_addr_); }

    constexpr const std::string& bus_subscriber_addr() const noexcept { return bus_sub_addr_; }
    std::string consume_bus_subscriber_addr() noexcept { return std::move(bus_sub_addr_); }

    constexpr std::size_t threads() const noexcept { return threads_; }
    constexpr std::size_t request_timeout() const noexcept { return request_timeout_; }

    constexpr bool wanted_help() const noexcept { return wanted_help_; }
    constexpr bool has_error() const noexcept { return has_error_; }
    constexpr bool use_bus() const noexcept { return use_bus_; }
    constexpr bool init_from_json() const noexcept { return init_from_json_; }
    constexpr const service& config() const noexcept { return config_; }

private:
    std::string broker_addr_;
    std::string balancer_addr_;
    std::string service_name_;
    std::string bus_pub_addr_;
    std::string bus_sub_addr_;
    std::size_t threads_;
    std::size_t request_timeout_;

    bool wanted_help_ = false;
    bool has_error_ = false;
    bool use_bus_ = false;

    bool init_from_json_ = false;
    service config_;
};

} // namespace pizza
