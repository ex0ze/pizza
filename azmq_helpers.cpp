/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "azmq_helpers.hpp"

namespace azmq_helpers
{

void recv_all(azmq::socket& sock, azmq::message&& msg, azmq::message_vector& storage)
{
    storage.clear();
    bool more = msg.more();
    storage.push_back(std::move(msg));
    while (more) {
        sock.receive(msg);
        more = msg.more();
        storage.push_back(std::move(msg));
    }
}

}
