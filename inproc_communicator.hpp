/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <boost/asio.hpp>
#include <condition_variable>
#include <mutex>
#include <thread>

#include "libs/cppzmq/zmq.hpp"

namespace pizza
{

template <const char* Addr, std::size_t QueueSize = 50'000>
class inproc_communicator {
public:
    inproc_communicator(zmq::context_t& ctx)
    : sock_(ctx, zmq::socket_type::pub)
    , guard_(boost::asio::make_work_guard(io_)) {
        boost::asio::post(io_, [this] {
            sock_.connect(inproc_addr());
        });
        io_thread_ = std::thread([this] { io_.run(); });
    }

    ~inproc_communicator() {
        running_ = false;
        guard_.reset();
        io_.stop();
        io_thread_.join();
    }

    void send(std::vector<zmq::message_t> data) {
        if (data.empty()) return;
        cv_check_in_progress();
        boost::asio::post(io_, [this, data = std::move(data)]() mutable {
            zmq::const_buffer empty;
            sock_.send(empty, zmq::send_flags::sndmore);
            for (std::size_t i = 0; i < data.size() - 1; ++i)
                sock_.send(std::move(data[0]), zmq::send_flags::sndmore);
            sock_.send(std::move(data.back()), zmq::send_flags::none);
            cv_notify();
        });
    }

    void send(zmq::message_t data) {
        cv_check_in_progress();
        boost::asio::post(io_, [this, data = std::move(data)]() mutable {
            --in_progress_;
            zmq::const_buffer empty(static_cast<const void*>(""), 1);
            sock_.send(empty, zmq::send_flags::sndmore);
            sock_.send(std::move(data), zmq::send_flags::none);
            cv_notify();
        });
    }

    void notify() {
        send(zmq::message_t());
    }

    static constexpr const char* inproc_addr() noexcept { return Addr; }

    static zmq::socket_t create_listener_socket(zmq::context_t& ctx) {
        zmq::socket_t socket(ctx, zmq::socket_type::sub);
        socket.set(zmq::sockopt::subscribe, zmq::const_buffer());
        socket.bind(inproc_communicator::inproc_addr());
        return socket;
    }

private:
    inline void cv_check_in_progress() {
        std::unique_lock<std::mutex> lock(busy_locker_);
        busy_.wait(lock, [this] { return in_progress_ < QueueSize || !running_; });
        ++in_progress_;
    }

    inline void cv_notify() {
        --in_progress_;
        busy_.notify_one();
    }

    zmq::socket_t sock_;
    boost::asio::io_context io_;
    std::thread io_thread_;
    boost::asio::executor_work_guard<boost::asio::io_context::executor_type> guard_;

    std::atomic_size_t in_progress_ = 0;
    std::condition_variable busy_;
    std::mutex busy_locker_;
    bool running_ = true;
};

} // namespace pizza
