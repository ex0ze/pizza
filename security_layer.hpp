/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <azmq/socket.hpp>
#include "libs/cppzmq/zmq.hpp"

namespace pizza
{

class service;

namespace detail
{

class security_layer {
public:
    virtual void set_security(const service&) = 0;
    
    using key = std::array<char, 41>;
    struct key_pair {
        key pub_key;
        key priv_key;
    };

    void install_keypair(azmq::socket& socket) {
        key_pair k;
        if (0 != zmq_curve_keypair(k.pub_key.data(), k.priv_key.data()))
            throw std::runtime_error("zmq_curve_keypair");
        socket.set_option(azmq::socket::curve_publickey(k.pub_key.data(), k.pub_key.size()));
        socket.set_option(azmq::socket::curve_privatekey(k.priv_key.data(), k.priv_key.size()));
    }
    void install_keypair(zmq::socket_t& socket) {
        key_pair k;
        if (0 != zmq_curve_keypair(k.pub_key.data(), k.priv_key.data()))
            throw std::runtime_error("zmq_curve_keypair");
        socket.set(zmq::sockopt::curve_publickey, std::string_view(k.pub_key.data(), k.pub_key.size()));
        socket.set(zmq::sockopt::curve_secretkey, std::string_view(k.priv_key.data(), k.priv_key.size()));
    }
};

} // namespace detail

} // namespace pizza
