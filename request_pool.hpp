/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <azmq/socket.hpp>
#include <boost/asio.hpp>

#include <chrono>
#include <deque>
#include <future>
#include <mutex>
#include <thread>
#include <vector>

#include "security_layer.hpp"

namespace pizza
{

class request_pool : public detail::security_layer {
public:
    using recv_type = std::vector<azmq::message>;
    using send_message_future = std::future<recv_type>;
    using callback_type = std::function<void(recv_type, std::exception_ptr)>;

private:

    struct timer_type {
        enum state {
            free,
            waiting,
            timed_out
        };
        timer_type(boost::asio::io_context& io)
            : timer(io) {}
        boost::asio::steady_timer timer;
        state st = free;
    };

    using work_guard = boost::asio::executor_work_guard<boost::asio::io_context::executor_type>;
    using socket_type = azmq::req_socket;
    using send_type = std::vector<std::string>;
    using shared_send_type = std::shared_ptr<send_type>;
    using send_message_promise = std::promise<recv_type>;
    using send_message_ret = std::variant<send_message_promise, callback_type>;
    using send_message_context_type = std::tuple<send_message_ret, shared_send_type, std::chrono::milliseconds>;

public:
    

    request_pool(std::size_t pool_size = 50);
    ~request_pool();

    request_pool& connect(std::string addr);

    virtual void set_security(const service& cfg) override;
    void set_security(std::string broker_key);
    void start();
    void stop();

    [[nodiscard]]
    send_message_future async_request(std::string service_name,
                                      std::string message,
                                      std::chrono::milliseconds timeout =
                                          std::chrono::milliseconds::max());

    void async_request(std::string service_name,
                       std::string message,
                       callback_type cb,
                       std::chrono::milliseconds timeout =
                           std::chrono::milliseconds::max());

private:
    static constexpr bool is_promise(send_message_context_type& ctx) noexcept {
        return std::get<send_message_ret>(ctx).index() == 0;
    }
    static constexpr bool is_callback(send_message_context_type& ctx) noexcept {
        return std::get<send_message_ret>(ctx).index() == 1;
    }
    static constexpr send_message_promise& get_promise(send_message_context_type& ctx) {
        return std::get<send_message_promise>(std::get<send_message_ret>(ctx));
    }

    static constexpr callback_type& get_callback(send_message_context_type& ctx) {
        return std::get<callback_type>(std::get<send_message_ret>(ctx));
    }

    static constexpr shared_send_type& get_data(send_message_context_type& ctx) noexcept {
        return std::get<shared_send_type>(ctx);
    }
    static constexpr std::chrono::milliseconds& get_timeout(send_message_context_type& ctx) noexcept {
        return std::get<std::chrono::milliseconds>(ctx);
    }

    inline void set_recv_type(send_message_context_type& ctx, recv_type & value);

    inline void set_exception(send_message_context_type& ctx, std::exception_ptr ex_ptr);

    void init_sockets(std::size_t pool_size);

    void enqueue_message(send_message_context_type && context);
    std::optional<send_message_context_type> dequeue_message();

    void cancel_ops();
    void check_queue(std::size_t socket_idx);
    void cancel_queue();
    void do_request(send_message_context_type && context, std::size_t socket_idx);

    void do_connect(azmq::socket& socket, std::string endpoint, bool reconnect);

    void set_op_timeout(std::size_t idx, std::chrono::milliseconds timeout = std::chrono::milliseconds::max());
    void cancel_op_timeout(std::size_t idx);
    inline bool op_timed_out(std::size_t idx) const noexcept;

    std::optional<std::size_t> get_free_socket();
    void add_free_socket(std::size_t idx);

    boost::asio::io_context io_;
    std::thread io_thread_;
    work_guard work_guard_;

    std::vector<socket_type> socket_pool_;
    std::deque<std::size_t> free_sockets_;
    std::vector<bool> busy_sockets_;
    std::vector<timer_type> timers_;
    std::deque<send_message_context_type> context_queue_;
    std::mutex lock_;
    std::condition_variable context_busy_;
    bool connected_ = false;
    bool started_ = false;

    struct {
        bool encryption = false;
        std::string broker_key;
    } security;

    static constexpr std::size_t queue_capacity = 50'000;
};

} // namespace pizza
