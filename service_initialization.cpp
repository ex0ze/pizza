/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "service_initialization.hpp"

#include <boost/program_options.hpp>

#include <iostream>
#include <sstream>
#include <thread>

#include <pizza-config/from_json.hpp>

namespace po = boost::program_options;

namespace pizza
{

command_line_args::command_line_args(int argc, const char * const argv[])
{
    po::options_description description("Service args");
    description.add_options()
        ("help,h", "Show this help")
        ("from-json", po::value<std::string>(), "Initialize service from json")
        ("from-stdin", "Read json config from stdin")
        ("broker-addr", po::value<std::string>(), "Address of the Broker")
        ("balancer-addr", po::value<std::string>(), "Address of the Balancer (can be equal to Broker address)")
        ("service-name", po::value<std::string>(), "Name of the service for network communications")
        ("threads", po::value<std::size_t>()->default_value(std::thread::hardware_concurrency()), "Number of execution threads")
        ("bus-pub-addr", po::value<std::string>(), "Address of the Bus Publisher (Leave it empty if you don't plan to use Bus)")
        ("bus-sub-addr", po::value<std::string>(), "Address of the Bus Subscriber (Leave it empty if you don't plan to use Bus)");
    po::parsed_options parsed(nullptr, 0);
    try {
        parsed = po::command_line_parser(argc, argv).options(description).allow_unregistered().run();
    }
    catch (const std::exception& e) {
        std::ostringstream stream;
        stream << e.what() << std::endl;
        description.print(stream);
        throw std::runtime_error(stream.str());
    }
    po::variables_map vm;
    po::store(parsed, vm);
    po::notify(vm);
    auto get_string_or_throw = [&vm, &description](const char * arg, std::string& str) {
        if (!vm.count(arg)) {
            std::ostringstream stream;
            description.print(stream);
            throw missing_argument_exception(std::string("Missing ") + arg + " argument", stream.str());
        }
        str = vm[arg].as<std::string>();
    };
    if (vm.count("help")) {
        description.print(std::cout);
        wanted_help_ = true;
        return;   
    }
    if (vm.count("from-stdin")) {
        config_ = from_stdin<service>();
        init_from_json_ = true;
        return;
    }
    if (vm.count("from-json")) {
        config_ = from_json<service>(vm["from-json"].as<std::string>());
        init_from_json_ = true;
        return;
    }
    get_string_or_throw("broker-addr", broker_addr_);

    if (!vm.count("balancer-addr"))
        balancer_addr_ = broker_addr_;
    else
        balancer_addr_ = vm["balancer-addr"].as<std::string>();
    
    get_string_or_throw("service-name", service_name_);
    if (vm.count("threads"))
        threads_ = vm["threads"].as<std::size_t>();
    if (vm.count("bus-pub-addr") || vm.count("bus-sub-addr")) {
        get_string_or_throw("bus-pub-addr", bus_pub_addr_);
        get_string_or_throw("bus-sub-addr", bus_sub_addr_);
        use_bus_ = true;
    }
}

} // namespace pizza
