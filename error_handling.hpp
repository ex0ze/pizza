/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <exception>
#include <functional>
#include <string_view>

namespace pizza
{

void print_exception_ptr(std::exception_ptr ex_ptr, std::ostream& os);

using generic_error_callback = std::function<void(std::exception_ptr, std::string_view /* module */)>;

generic_error_callback default_generic_error_callback();

enum class network_op_type {
    op_recv,
    op_send,
    op_conn
};

using network_error_callback = std::function<void(network_op_type, std::exception_ptr, std::string_view /* module */)>;

network_error_callback default_network_error_callback();

}
