/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <unordered_map>

#include "libs/cppzmq/zmq.hpp"

#include <pizza-config/requester.hpp>

#include "bus_link.hpp"
#include "contract_request_pool.hpp"

namespace pizza
{

using request_pool_map = std::unordered_map<std::string, std::shared_ptr<contract_request_pool>>;
using bus_map = std::unordered_map<std::string, std::shared_ptr<bus_link>>;

request_pool_map make_request_pool_map(const requester& r, std::size_t request_pool_sockets = 50);

bus_map make_bus_map(const requester& r, zmq::context_t& ctx, boost::asio::thread_pool& thread_pool);

}
