function(generate_protobuf PROTO_PATH PROTO_HEADERS PROTO_SOURCES)
    file(GLOB ProtoFiles "${PROTO_PATH}/*.proto")

    foreach(ProtoFile ${ProtoFiles})
       get_filename_component(ProtoName ${ProtoFile} NAME_WE)
       execute_process(COMMAND ${PROTOBUF_PROTOC_EXECUTABLE} --proto_path=${PROTO_PATH} --cpp_out=${PROTO_PATH} ${ProtoFile}
       RESULT_VARIABLE ProtoGenResult)
       set(RelativeProtoHeader ${ProtoName}.pb.h)
       set(RelativeProtoSource ${ProtoName}.pb.cc)
   
       set(CurrentProtoHeader ${PROTO_PATH}/${RelativeProtoHeader})
       set(CurrentProtoSource ${PROTO_PATH}/${RelativeProtoSource})
       if(${ProtoGenResult})
           message("Error generating proto file ${ProtoFile}")
       else()
           message("${ProtoFile} -> ${RelativeProtoHeader} ${RelativeProtoSource}")
           set(ProtoHeaders
               ${ProtoHeaders}
               ${CurrentProtoHeader})
           set(ProtoSources
               ${ProtoSources}
               ${CurrentProtoSource})
           set(${PROTO_HEADERS} ${ProtoHeaders} PARENT_SCOPE)
           set(${PROTO_SOURCES} ${ProtoSources} PARENT_SCOPE)
       endif()
    endforeach()
endfunction()
