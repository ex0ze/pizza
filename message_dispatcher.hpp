/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <string>

#include "libs/cppzmq/zmq.hpp"

#include <google/protobuf/any.h>
#include <google/protobuf/any.pb.h>

namespace pizza
{

template <typename Delegate, typename Message, typename ... Contracts>
struct message_dispatcher {
    message_dispatcher(Delegate& delegate) : delegate_(delegate) { }
    void deserialize(zmq::message_t&& identity, zmq::message_t&& data) {
        Message msg;
        if (!msg.ParseFromArray(data.data(), data.size()))
            return;
        dispatch(std::move(identity), std::move(msg));
    }

private:
    void dispatch(zmq::message_t&& identity, Message&& msg) {
        assert(msg.mutable_message());
        ::google::protobuf::Any& any_message = *msg.mutable_message();
        if (!(dispatch_impl<Contracts>(identity, any_message) || ... || false)) {
            return;
        }
    }
    template <typename Contract>
    bool dispatch_impl(zmq::message_t& identity, const ::google::protobuf::Any& message) {
        if (message.Is<typename Contract::request_type>()) {
            typename Contract::request_type concrete_request;
            if (!message.UnpackTo(&concrete_request)) {
                return false;
            }
            typename Contract::response_type concrete_response;
            delegate_.process_message(std::move(concrete_request), concrete_response);
            zmq::message_t serialized_message(concrete_response.ByteSizeLong());
            concrete_response.SerializeToArray(serialized_message.data(), serialized_message.size());
            delegate_.message_processed(std::move(identity), std::move(serialized_message));
            return true;
        }
        return false;
    }

    Delegate& delegate_;
};

} // namespace pizza
